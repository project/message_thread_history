INTRODUCTION
------------

The Message Thread History module records when a message within a thread has been added.  It marks message threads as
 new depending on the last time the user viewed the thread.


REQUIREMENTS
------------

This module requires the following modules:

 * History (included in Drupal core)
 * Message History (https://www.drupal.org/project/message_history)
 * Message Thread (https://www.drupal.org/project/message_thread)


INSTALLATION
------------

Install as usual, see https://www.drupal.org/docs/8/extending-drupal-8/installing-modules 
for further information.
